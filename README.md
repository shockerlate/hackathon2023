# Hackathon2023

## Get started

git clone https://gitlab.com/shockerlate/hackathon2023.git

### Frontend

cd hackathon2023

cd frontend

npm install

npm run serve

### Backend

cd hackathon2023

cd backend

npm install

node index.js