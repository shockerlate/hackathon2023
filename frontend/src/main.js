import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import axios from 'axios' //เพิ่ม
import VueAxios from 'vue-axios' //เพิ่ม

Vue.config.productionTip = false
Vue.use(VueAxios, axios) //เพิ่ม

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
