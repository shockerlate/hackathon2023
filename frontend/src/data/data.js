var ZeroWaste = [
    {
        "name": 'Reduce single-use plastics',
        "image": 'zerowaste_1.png',
        "detail": [
            'Reusable water bottles',
            'Reusable bags',
            'Reusable containers',
            'Say no to straws'
        ],
    },
    {
        "name": 'Sustainable transportation',
        "image": 'zerowaste_2.png',
        "detail": [
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
    {
        "name": 'Reduce energy consumption',
        "image": 'zerowaste_3.png',
        "detail": [
            'Reusable water bottles',
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
    {
        "name": 'Sustainable shopping habits',
        "image": 'zerowaste_4.png',
        "detail": [
            'Reusable water bottles',
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
    {
        "name": 'Shop secondhand',
        "image": 'zerowaste_5.png',
        "detail": [
            'Reusable water bottles',
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
    {
        "name": 'Donate and Repurpose',
        "image": 'zerowaste_6.png',
        "detail": [
            'Reusable water bottles',
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
    {
        "name": 'Recycling',
        "image": 'zerowaste_7.png',
        "detail": [
            'Reusable water bottles',
            'Use compost as natural fertilizer for plants and gardens',
        ],
    },
];
module.exports = ZeroWaste;
